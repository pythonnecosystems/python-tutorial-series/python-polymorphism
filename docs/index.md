# Python 다형성: 메서드 오버로딩, 오버라이딩 <sup>[1](#footnote_1)</sup>

<font size="3">Python OOP에서 메소드의 동작을 변경하기 위해 다형성을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./polymorphism.md#intro)
1. [다형성이란?](./polymorphism.md#sec_02)
1. [Python에서 메서드 오버로딩](./polymorphism.md#sec_03)
1. [Python에서 메서드 오버라이딩](./polymorphism.md#sec_04)
1. [Python의 다형성 사례](./polymorphism.md#sec_05)
1. [요약](./polymorphism.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 23 — Python Polymorphism: Method Overloading, Overriding](https://levelup.gitconnected.com/python-tutorial-23-python-polymorphism-method-overloading-overriding-1d73f0d0c171)를 편역하였다.
